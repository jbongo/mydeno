const server = require('./src/app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let chaiFiles = require('chai-files');


let expect = chai.expect;
let file = chaiFiles.file;

chai.should();

chai.use(chaiFiles);
chai.use(chaiHttp);


describe('Liste des étudiants', () => {

    it('Doit retourner un tableau et un code de statut 200 ', (done) => {
        chai.request(server)
            .get('/get-etudiants')
            .end((err, res) => {
                res.body.should.be.a('array');
                res.should.have.status(200);
                done();
            });

    });

});

describe('Ajout étudiant', () => {

    it('Vérifie que le fichier data.csv exist et contient test ', (done) => {
        chai.request(server)
            .post('/add-etudiant')
            .type('form')
            .send({ "nom": "test", "note": 10 })
            .end((err, res) => {
                expect(file('data.csv')).to.exist;
                expect(file('data.csv')).to.contain('test');

                done();
            });

    });

});


describe('Sprresion d\'un étudiant', () => {

    it('vérifie que le fichier data.csv ne contient plus test ', (done) => {
        chai.request(server)
            .delete('/etudiant/delete/test')
            .type('form')
            .end((err, res) => {
                expect(file('data.csv')).to.not.contain('test');
                done();
            });

    });

});