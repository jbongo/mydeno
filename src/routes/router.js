const path = require("path");
const etudiantController = require('../Controllers/etudiantController');



const router = (app) => {

    // Page d'acceuil
    app.get('/', etudiantController.accueilEtudiants);


    // Retourne la liste des étudiants
    app.get('/get-etudiants', etudiantController.getEtudiants);

    // Page d'ajout étudiant
    app.get('/add-etudiant', etudiantController.ajoutEtudiant);


    // Ajouter un étudiant
    app.post('/add-etudiant', etudiantController.addEtudiant);


    app.delete('/etudiant/delete/:nom', etudiantController.deleteEtudiant);


}

module.exports = router;