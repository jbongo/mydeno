const fs = require('fs');
const path = require("path");



/**Ajout d'un étudiant */

const addEtudiant = (req, res) => {


    const newEtudiant = req.body;

    // Lecture du fichier data.csv
    let etudiants = getEtudiants(req, res);

    // res.send(etudiants);

    etudiants.push(newEtudiant);

    // Ajout de la nouvelle data dans le fichier
    fs.writeFileSync('data.csv', JSON.stringify(etudiants), (error) => {
        if (error) throw err;
    });

    // nouvelle ressource crée  
    res.status(201);

}


/**Retourne la liste des étudiants */

const getEtudiants = (req, res) => {

    let etudiants = fs.readFileSync('data.csv', 'utf8', (err) => {
        if (err) throw err;
    });

    etudiants = JSON.parse(etudiants);

    // requête exécutée avec succès 
    res.send(etudiants);

    return etudiants;

}


/**Suppression d'un étudiant */

const deleteEtudiant = (req, res) => {

    let etudiants = getEtudiants(req, res);

    etudiants = etudiants.filter(etud => etud.nom != req.params.nom);


    // Ajout de la nouvelle data dans le fichier
    fs.writeFileSync('data.csv', JSON.stringify(etudiants), (error) => {
        if (error) throw err;
    });

    // ressource supprimée avec succès 
    res.status(204);

}


/**Affiche la page d'accueil */

const accueilEtudiants = (req, res) => {
    res.sendFile(path.join(__dirname + '/../view/index.html'));
}


/**Affiche la page de création d'étudiant */

const ajoutEtudiant = (req, res) => {
    res.sendFile(path.join(__dirname + '/../view/add-etudiant.html'));
}





module.exports = {
    addEtudiant,
    getEtudiants,
    accueilEtudiants,
    ajoutEtudiant,
    deleteEtudiant
};